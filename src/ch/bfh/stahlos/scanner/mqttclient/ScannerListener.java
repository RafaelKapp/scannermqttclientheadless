package ch.bfh.stahlos.scanner.mqttclient;

import java.util.Scanner;

/**
 * The Listenerclass
 * 
 * Listen to all what the Scanner is giving, and give it
 * to the MqttScannerClient
 * 
 * @author Kappadona
 *
 */
public class ScannerListener{

	//New instance of MqttScannerClient
	private MqttScannerClient scannerClient = new MqttScannerClient();

	/**
	 * We just need the constructor to do all what we want:
	 * 
	 * Do the connection to mqtt broker
	 * 
	 */
	public ScannerListener(){
		//Do the connection to mqtt broker
		scannerClient.doConnection();
		//Take a scanner instance from java.util and listen to System input
		@SuppressWarnings("resource")
		Scanner scan= new Scanner(System.in);

		
		while(true){
			//Read the input from Scanner
			String text= scan.nextLine();
			//Send it to the broker
			scannerClient.sendCodeStream(text); 
		}
	}
	
	//in the main methode we just start the ScannerListener constructor
	public static void main(String[] args) {
		new ScannerListener();
	}
}
