package ch.bfh.stahlos.scanner.mqttclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * This class handle the mqtt for the scanners from STAHLOS
 * 
 * implements the MqttCallback from eclipse.paho
 * @author Kappadona
 *
 */
public class MqttScannerClient implements MqttCallback{
	//we need an instance of MqttClient from eclipse.paho
	private MqttClient client;
	//Define a standard char set
	final static Charset ENCODING = StandardCharsets.UTF_8;
	
	//Give a topic name for publishing messages
	private String topicName = "WaitingroomScannerEvent";
	
	//Mqtt client name, with this name you will be known on the broker side
	private String clientName = "Waitingroom Client";
	
	//boolean for error handling while sending messages
	private boolean openCodeStream = false;
	//String for error handling while sending messages
	private String openCodeStreamString = "";
	
	
	/**
	 * This method is used to open a connection with the broker
	 */
	public void doConnection(){		
		//open instance with StringWriter
		StringWriter writer = new StringWriter();
		
        try {
        	//Use this for use in Raspberry Pi
        	String raspberryPath = "/home/pi/Desktop/STAHLOS/config.txt";
        	//Use this for testing on your local development envionment
//        	String localPath = "resources/config.txt";

        	//Copy the input from the configuration file into the writer
			IOUtils.copy(new FileInputStream(new File(raspberryPath)), writer, "UTF-8");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        //writing the input from the writer into the string
        String configInput = writer.toString();
        
        //splitting the whole file into different lines as an array
        String configInputLines[] = configInput.split("\\r?\\n");
		
        //setup brokerIp and brokerPort String to get filled
		String brokerIp = "";
		String brokerPort = "";
		
		//read the file and search for the ip and the port from the config.txt
		//and write it into the Strings
		for(String line: configInputLines){
			if(line.startsWith("IP:"))
				brokerIp = line.substring(3);		//String just after the "IP:"
			if(line.startsWith("Port:"))
				brokerPort = line.substring(5);		//String just after the "Port:"
		}
		
		//if there is an empty file or the config.txt is invalid, we print a message
		if(brokerIp.isEmpty() || brokerPort.isEmpty()){
			System.out.println("Invalide File: config.txt");
		}
		
		//now we are ready to try to connecting with the broker
		try {
			//configure the MqttClient with the ip, the port and giving a client name
			client = new MqttClient(
					"tcp://" + brokerIp + ":" + brokerPort, 
					clientName,
					new MemoryPersistence()
					);
			//connect
	        client.connect();
	        
			System.out.println("connected");
			
			//if we still have an not sent code we will do it now
			if(openCodeStream){
				sendCodeStream(openCodeStreamString);
				openCodeStream = false;
				System.out.println("Message sent!");
			}
			
		} catch (MqttException e) {
			e.printStackTrace();
			//in case of an exception while connecting, we try again
			doConnection();
		}
	}
	
	/**
	 * This method is used to send a string to the broker
	 * @param codeStream
	 */
	public void sendCodeStream(String codeStream){
		//giving this instance as callback to the MqttClient
		client.setCallback(this);
		//make new instance of MqttMessage
        MqttMessage message = new MqttMessage();
        //set the content from the message
        message.setPayload(codeStream
                .getBytes());
        //set Quality of Service to level 2
        message.setQos(2);
        //now we try to publish our message to the broker
        try {
        	//we need to give a topic name, and ofcourse the message
			client.publish(topicName, message);
		} catch (MqttException e) {
			e.printStackTrace();
			
			//Oups something went wrong, we try to reconnect and send after
			System.out.println("Message sending ERROR");
			doConnection();
			openCodeStream = true;
			openCodeStreamString = codeStream;
		}
	}

	/**
	 * will be called if the connection is lost
	 */
	@Override
	public void connectionLost(Throwable arg0) {
		//Connection lost, we try to connect again! This happen automatically
		System.out.println("Connection lost");
		System.out.println("Try to reconnect!");
	}

	//We don't need those methods from the interface
	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {}
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {}
	
	
	//open the constructor of ScannerListener to start the service
	public static void main(String[] args) {
		new ScannerListener();
	}
}
